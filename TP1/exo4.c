#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#define MAXVAL 1000000

int main(int argc, char *argv[]) {
    if(argc < 3) {
        printf("Usage : %s <taille_tableau> <nombre_threads>\n", argv[0]);
        return 1;
    }

    double time_start, time_end, time_seq, somme_seq = 0;
    double time_start_p, time_end_p, time_p, somme_p = 0;
    double time_start_red, time_end_red, time_red, somme_red = 0;


    omp_set_num_threads(atoi(argv[2]));
    srand(getpid());// init. générateur aléatoire
    
    //////////
    // INIT //
    //////////

    int taille = atoi(argv[1]);
    
    double *tt = malloc(sizeof(double) * taille);
    if(tt == NULL){
        printf("Erreur malloc\n");
        return 1;
    }

    for(int i=0; i<taille; i++) {
        tt[i] = rand() % MAXVAL;// de 0 à MAXVAL exclu
    }


    ////////////////////////////
    // EXECUTION SEQUENTIELLE //
    ////////////////////////////

    printf("=== Séquentiel ===\n");

    time_start = omp_get_wtime();

    for(int i=0; i<taille; i++){
        somme_seq += tt[i];
    }

    time_end = omp_get_wtime();
    time_seq = time_end - time_start;
    printf("Somme : %lf\n", somme_seq);
    printf("Temps : %lf\n", time_seq);
    

    /////////////////////////
    // EXECUTION PARALLELE //
    /////////////////////////

    printf("=== Parallèle ===\n");

    time_start_p = omp_get_wtime();

    #pragma omp parallel for
    for(int i=0; i<taille; i++){
        #pragma omp atomic
        somme_p += tt[i];
    }

    time_end_p = omp_get_wtime();
    time_p = time_end_p - time_start_p;

    printf("Somme : %lf\n", somme_p);
    printf("Temps : %lf\n", time_p);

    // la somme n'est pas bonne à cause les écritures mémoire
    // avec pragma omp atomic, le temps en beaucoup plus long, mais la somme est exacte

    ////////////////////////////////////////
    // EXECUTION PARALLELE AVEC REDUCTION //
    ////////////////////////////////////////

    printf("=== Parallèle avec réduction ===\n");

    time_start_red = omp_get_wtime();

    #pragma omp parallel for reduction(+:somme_red) schedule(dynamic,100000)
    for(int i=0; i<taille; i++){
        somme_red += tt[i];
    }

    time_end_red = omp_get_wtime();
    time_red = time_end_red - time_start_red;

    printf("Somme : %lf\n", somme_red);
    printf("Temps : %lf\n", time_red);

    // une plus grande taille de chunks augmente le speedup jusqu'à un certain seuil)

    /////////////////////////    

    printf("\n=== Speedups ===\n");

    printf("Speedup séquentielle/parallèle : %lf\n", time_seq/time_p);    
    printf("Speedup séquentielle/réduction : %lf\n", time_seq/time_red);    

    // le speedup augmente avec n

    free(tt);

    return EXIT_SUCCESS;
}