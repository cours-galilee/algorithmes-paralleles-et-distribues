#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>


#define MAXVAL 10000

float f1(float x){
    return 1.17*x*x;
}
float f2(float x){
    return 2.17*log(x)*cos(x);
}


int main(int argc, char *argv[]) {
    if(argc < 3) {
        printf("Usage : %s <taille_tableau> <nombre_threads>\n", argv[0]);
        return 1;
    }

    double time_start, time_end, time_seq;
    double time_start_p, time_end_p, time_p;


    omp_set_num_threads(atoi(argv[2]));
    srand(getpid());// init. générateur aléatoire
    
    //////////
    // INIT //
    //////////

    int taille = atoi(argv[1]);
    
    double *tt = malloc(sizeof(double) * taille);
    if(tt == NULL){
        printf("Erreur malloc\n");
        return 1;
    }

    for(int i=0; i<taille; i++) {
        tt[i] = rand() % MAXVAL;// de 0 à MAXVAL exclu
    }


    ////////////////////////////
    // EXECUTION SEQUENTIELLE //
    ////////////////////////////

    time_start = omp_get_wtime();

    for(int i=0; i<taille; i++){
        f2(tt[i]);
    }

    time_end = omp_get_wtime();
    time_seq = time_end - time_start;
    printf("Temps : %lf\n", time_seq);
    

    /////////////////////////
    // EXECUTION PARALLELE //
    /////////////////////////

    time_start_p = omp_get_wtime();

    #pragma omp parallel for
    for(int i=0; i<taille; i++){
        f2(tt[i]);
    }

    time_end_p = omp_get_wtime();
    time_p = time_end_p - time_start_p;
    printf("Temps parallèle : %lf\n", time_p);

    /////////////////////////    

    printf("======\nSpeedup : %lf\n", time_seq/time_p);    

    free(tt);

    return EXIT_SUCCESS;
}