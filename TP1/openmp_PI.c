#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>

#define NBITER 100000000

double f(double x) {
	return(4.0/(1.0+x*x));
}

// ATTENTION ce code contient une erreur !
double calcul_pi_parallele(int nb_threads, double a, double b, long int N){
  long int i;
  double valeur_pi = 0.0;
  double somme = 0.0;
  double xi, h = (b-a)/N;

  #pragma omp parallel for num_threads(nb_threads) private(xi) reduction(+:somme)
  for (i = 0; i < N; i++) {
    xi = a+h*i;
		somme += f(xi);
  }

  valeur_pi = h * (somme+(f(a)+f(b))/2);
  return valeur_pi;
}

int main(int argc, char **argv){   
  int nb;
  double resu;
  double start,end;

  //printf("\n\n*** Attention ce code contient une erreur ! ***\n\n");
  printf("\nNb.threads\tTemps\tEstimation de Pi\n");

  for(nb=1 ; nb <= 12 ; nb++){
    printf("%d\t",nb);
    fflush(stdout);
	  
    start = omp_get_wtime();
    resu = calcul_pi_parallele(nb, 0, 1, NBITER);
    end = omp_get_wtime();
    
    printf("\t%1.4lf\t%1.12lf\n", end-start, resu);
  }

  return EXIT_SUCCESS;
} 
