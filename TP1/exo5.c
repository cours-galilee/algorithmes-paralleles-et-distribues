#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#define MAXVAL 100


int** creationMat(int m) {
    int **mat = malloc(sizeof(int*) * m);
    if(mat == NULL){
        return NULL;
    }

    for(int i=0; i<m; i++) {
        int* ligne = malloc(sizeof(int) * m);
        if(mat == NULL){
            return NULL;
        }

        mat[i] = ligne;
        for(int j=0; j<m; j++){
            mat[i][j] = rand() % MAXVAL;// de 0 à MAXVAL exclu
        }
    }

    return mat;
}

void freeMat(int** mat, int m) {
    for(int i=0; i<m; i++) {
        free(mat[i]);
    }
    free(mat);
}

void printMat(int** mat, int m){
    for(int i=0; i<m; i++){
        for(int j=0; j<m; j++){
            printf("%3d ", mat[i][j]);
        }
        printf("\n");
    }
}

int** sommeMat(int** A, int** B, int m){
    int** C = creationMat(m);

    #pragma omp parallel for
    for(int k=0; k<m*m; k++){
        int i = k/m;
        int j = k%m;
        C[i][j] = A[i][j] + B[i][j];
    }

    return C;int
}

int main(int argc, char *argv[]) {

    if(argc < 3) {
        printf("Usage : %s <taille_tableau> <nombre_threads>\n", argv[0]);
        return 1;
    }

    double time_start, time_end, time;


    omp_set_num_threads(atoi(argv[2]));
    srand(getpid());// init. générateur aléatoire
    
    //////////
    // INIT //
    //////////

    int taille = atoi(argv[1]);
    
    int **mat1 = creationMat(taille);
    int **mat2 = creationMat(taille);

    time_start = omp_get_wtime();

    int** s = sommeMat(mat1, mat2, taille);

    time_end = omp_get_wtime();
    
    printf("Temps : %lf\n", time_end - time_start);

    //printMat(mat1, taille);
    //printf("\n");
    //printMat(mat2, taille);
    //printf("\n");
    //printMat(s, taille);


    freeMat(mat1,taille);
    freeMat(mat2,taille);
    freeMat(s,taille);

    return EXIT_SUCCESS;
}