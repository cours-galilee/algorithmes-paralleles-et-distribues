#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main() {
    #ifdef _OPENMP

    int nbProc = omp_get_num_procs();
    int nbThreads = omp_get_max_threads();
    int nbThreadsLimit = omp_get_thread_limit();
    int gestionDynamique = omp_get_dynamic();
    int nested = omp_get_nested();
    

    printf("Nombre de processeurs : %d\n", nbProc);
    printf("Nombre max de threads : %d\n", nbThreads);
    printf("Nombre de threads limite utilisable par OpenMP : %d\n", nbThreadsLimit);


    if(gestionDynamique) {
        printf("Gestion dynamique : activée\n");
    } else {
        printf("Gestion dynamique : désactivée par défaut\n");
        omp_set_dynamic(1);
        if(omp_get_dynamic()){
            printf("Gestion dynamique après changement : activée\n");
        }
    }

    if(nested) {
        printf("Gestion des boucles parallèles imbiquées : activée\n");
    } else {
        printf("Gestion des boucles parallèles imbiquées : désactivée par défaut\n");
        omp_set_nested(1);
        if(omp_get_nested()){
            printf("Gestion des boucles parallèles imbiquées après changement : activée\n");
        }
    }


    
    printf("Version OpenMP : %d\n", _OPENMP);

    #pragma omp parallel
    {
        printf("Nombre de threads de la section parallèle : %d\n", omp_get_num_threads());
        printf("Numéro du thread : %d\n", omp_get_thread_num());
    }

    #endif

    return EXIT_SUCCESS;
}