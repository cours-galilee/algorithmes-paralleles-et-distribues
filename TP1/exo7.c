#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

typedef struct Element {
    double val;
    struct Element* next;
} Element;

typedef Element* Liste;

double f(double x) {
	return(4.0/(1.0+x*x));
}


double pi(){
    double a=0.0, b=1.0;
    long int i;
    int N = 100000000;
    double valeur_pi = 0.0;
    double somme = 0.0;
    double h = (b-a)/N;

    for (i = 0; i < N; i++) {
        somme += f(a+h*i);
    }

    valeur_pi = h * (somme+(f(a)+f(b))/2);
    return valeur_pi;
}

void parcours_liste(Liste l) {
    Element* e;
    for (e = l ; e != NULL ; e = e->next)
        #pragma omp task
        e->val = pi();

    /*attendre fin des tâches filles*/
    #pragma omp taskwait
}

int main () {
    Liste l = NULL;
    /*Ici création de la liste*/

    for(int i=0; i<20; i++){
        Element* e = malloc(sizeof(Element));
        e->next = l;
        l = e;
    }
    
    #pragma omp parallel
    {
        /*Un seul thread va créer toutes
         les tâches qui seront exécutées en
        parallèle*/ 
        #pragma omp single
        parcours_liste(l);
    }

    for (Element* e = l ; e != NULL ; e = e->next){
        printf("%lf\n", e->val);
    }
}