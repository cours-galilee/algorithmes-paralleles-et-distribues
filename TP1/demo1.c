/*  demo OpenMP sur boucle for : simple affichage de qui fait quoi 
 *  nécessite l'option de compilation -lm pour la lib math */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

/* petit tableau pour pouvoir voir quelque chose */
#define MAXARRAY 320

/* Une fonction quelconque pour prendre un peu de temps */
double mafunc(int x) {
	x=x+1;
	return  sin(100*cos(log(x*17)*log(x*23)));
}

int main (int argc, char *argv[]) {
	int numTh;
    int *tab = malloc (sizeof (int) * MAXARRAY);
	int maxTh=omp_get_max_threads();

	/* nbCases[t] donne le nbre de cases traitées par le thread t */
	int *nbCases = calloc(maxTh, sizeof(int));
	/* casesTraites[t] : tableau des cases traitées par le thread t */
	int **casesTraites = malloc(sizeof(int *) * maxTh);

	for (int i=0; i< maxTh; i++) {
		casesTraites[i] = malloc(sizeof(int) * MAXARRAY);
	}

	// avec runtime : choix par variable d'environnement OMP_SCHEDULE
	// sans recompilation.
	// par exemple :  export OMP_SCHEDULE=static,10
    #pragma omp parallel for private(numTh) schedule(runtime)
    for (int i = 0; i < MAXARRAY; i++) {
		/* ici une boucle de calcul pour un exemple de temps
		 * non régulier suivant les valeurs de i */
		for (int j=0 ; j < i ; j++) { 
        	tab[i] += mafunc(j); // Juste un exemple de calcul
		}
		numTh = omp_get_thread_num();
		casesTraites[numTh][nbCases[numTh]] = i;
		nbCases[numTh]++;
	}

	for (int t=0; t< maxTh ; t++) {
		printf ("Thread %2d (%2d cases):",t, nbCases[t]);
		for (int j=0; j< nbCases[t] ; j++) {
			printf("%2d,",casesTraites[t][j]);
		}
		printf("\n");
	}

	printf("\n");
    return (0);
}
