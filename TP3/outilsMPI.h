/* outilsMPI.h
 * quelques macros utiles */

#ifndef outilsMPI_h
#define outilsMPI_h 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <mpi.h>

/* affiche erreur et quitte proprement */
#define erreur(message)     ({ if (0==rank) fprintf(stderr,message); \
                    MPI_Finalize(); \
                    exit(EXIT_FAILURE); })

/* On définit le calcul de la moyenne comme une macro pour ne pas avoir à faire
 * deux fonctions identiques mais de type diff..  */
#define moyenneTab(t,taille)   ({\
            double _s=0; \
            for (int i=0; i< taille; i++) _s += t[i]; \
            _s/(taille); \
        })

/* idem pour le max, possible de faire mieux avec typeof ou __auto_type avec gcc
 * pour que le résultat soit du même type que les cases du tableau, tant pis. */
#define maxTab(t,taille)   ({\
			double _s=t[0]; \
			for (int i=1; i< taille; i++) _s = (_s > t[i]) ? _s : t[i]; \
			_s; \
		})

/* idem pour max de matrice 2D stockée classiquement */
#define maxTab2D(t,nbl,nbc) ({\
			double _s=t[0][0]; \
			for (int i=0; i< nbl; i++) \
				for (int j=0 ; j<nbc; j++) _s = (_s > t[i][j]) ? _s : t[i][j]; \
			_s; \
		})

/* de même pour initialiser des tableaux de valeurs aleatoires entre min et max */
#define initTabRand(tab,taille,seed,min,max) ({\
            srand(seed); \
            for (int i=0;i <taille; i++) \
				tab[i] = rand()%(max-min)+min; \
		})

/* idem 2D */
#define initTab2DRand(tab,nbl,nbc,seed,min,max) ({\
			srand(seed); \
			for (int i=0;i <nbl; i++) \
				for (int j=0;j <nbc; j++) \
					tab[i][j] = rand()%(max-min)+min;\
		})

/* Affiche matrice stoquée linéairement */
#define afficheTabInt(tab,nbl,nbc) ({      	\
			for (int i=0;i <nbl*nbc; i++) {    	\
				if ((i%nbc)==0) printf("\n"); 	\
				printf("%6d",tab[i]); 			\
			} 									\
			printf("\n"); 						\
		})

/* Affiche matrice 2D stoquée classiquement, peut  être utilisée avec des 
 * matrices d'entiers ou double suivant param. format */
#define afficheTab2D(format,tab,nbl,nbc) ({\
			for (int i=0;i <nbl; i++) {\
				for (int j=0; j<nbc; j++) \
					printf(format,tab[i][j]); \
				printf("\n"); \
			} \
		})
#endif //outilsMPI_h
