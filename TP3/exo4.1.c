#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "outilsMPI.h"

#define M 10
#define N 4

int main(int argc, char**argv) {
    int rang, nbProcs, tag=0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);
    srand(time(NULL) + rang);

    int voisin;
    int matrice[M*N];
    int position;

    initTabRand(matrice, M*N, rang+1, 0, 100);

    afficheTabInt(matrice, M, N);

    if(rang == 0){
        voisin = 1;
        for(int i=0; i<M; i++){
            for(int j=0; j<2; j++){
                position = i*N + j;
                MPI_Send(matrice + position, 1, MPI_INT, voisin, tag, MPI_COMM_WORLD);
                MPI_Recv(matrice + position, 1, MPI_INT, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        }

    } else if(rang ==1){
        voisin = 0;
        int tmp;

        for(int i=0; i<M; i++){
            for(int j=0; j<2; j++){
                position = i*N + j;
                MPI_Recv(&tmp              , 1, MPI_INT, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Send(matrice + position, 1, MPI_INT, voisin, tag, MPI_COMM_WORLD);
                matrice[position] = tmp;
            }
        }
    }

    //nombre message envoyé : M
    //nombre total : 2*M

    afficheTabInt(matrice, M, N);

    printf("Fin\n");
    MPI_Finalize();    
    return EXIT_SUCCESS;
}