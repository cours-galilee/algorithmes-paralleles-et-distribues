#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char**argv) {
    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

    char buffer[MPI_MAX_PROCESSOR_NAME];
    int tailleChaine;
    MPI_Get_processor_name(buffer, &tailleChaine);

    MPI_Bcast(&buffer, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);
    
    if(rang == 0) {
	    printf("Je suis le processus 0 de nom '%s', je l'envoie à tous\n", buffer);
    } else {
        printf("Le processus 0 est '%s'\n", buffer);
    }
    
    printf("Fin\n");

    MPI_Finalize();
    
    return EXIT_SUCCESS;
}