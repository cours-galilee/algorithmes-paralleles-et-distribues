#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define M 3
#define N 3

int randInterval(int min, int max){
    return (rand() % (max-min+1)) + min;
}

int main(int argc, char**argv) {
    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);
    srand(time(NULL) + rang);


    double matrice[M][N];
    double maxMatrice=0.0, maxAll;

    //init matrice
    for(int i=0; i<M; i++){
        for(int j=0; j<N; j++){
            matrice[i][j] = randInterval(0, 100);
        }
    }

    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            printf("%lf ", matrice[i][j]);
        }
        printf("\n");
    }

    // max
    for(int i=0; i<M; i++){
        for(int j=0; j<N; j++){
            if(maxMatrice < matrice[i][j]) {
                maxMatrice = matrice[i][j];
            }
        }
    }
    printf("Max local : %lf\n", maxMatrice);

    // Partage du max
    MPI_Allreduce(&maxMatrice, &maxAll, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    printf("Max global : %lf\n", maxAll);

    // Normalisation
    for(int i=0; i<M; i++){
        for(int j=0; j<N; j++){
            matrice[i][j] /= maxAll;
        }
    }
    
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            printf("%lf ", matrice[i][j]);
        }
        printf("\n");
    }

    printf("Fin\n");
    MPI_Finalize();    
    return EXIT_SUCCESS;
}