#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "outilsMPI.h"

#define M 10
#define N 4

int main(int argc, char**argv) {
    int rang, nbProcs, tag=0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);
    srand(time(NULL) + rang);

    int voisin;
    int matrice[M*N];
    MPI_Datatype type_colonne;

    //Init du type
    MPI_Type_vector(M, 1, N, MPI_INT, &type_colonne);
    MPI_Type_commit(&type_colonne);

    printf("Début\n");

    //initTabRand(matrice, M*N, rang+1, 0, 100);
    for(int i=0; i<M; i++){
        for(int j=0; j<N; j++) {
            matrice[i*N+j] = (rang+1)*10000 + (i+1)*10 + j+1;
        }
    }

    printf("Fin Init\n");


    afficheTabInt(matrice, M, N);

    if(rang == 0){
        voisin = 1;
        MPI_Send(matrice    , 1, type_colonne, voisin, tag, MPI_COMM_WORLD);
        MPI_Send(matrice + 1, 1, type_colonne, voisin, tag, MPI_COMM_WORLD);
        MPI_Recv(matrice    , 1, type_colonne, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(matrice + 1, 1, type_colonne, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    } else if(rang ==1){
        voisin = 0;
        int tmp[M*N];
        int position;

        MPI_Recv(tmp        , 1, type_colonne, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(tmp + 1    , 1, type_colonne, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(matrice    , 1, type_colonne, voisin, tag, MPI_COMM_WORLD);
        MPI_Send(matrice + 1, 1, type_colonne, voisin, tag, MPI_COMM_WORLD);
        
        for(int i=0; i<M; i++){
            for(int j=0; j<2; j++){
                position = (i*N)+j;
                matrice[position] = tmp[position];
            }
        }
    }

    //nombre message envoyé : 2
    //nombre total : 4

    afficheTabInt(matrice, M, N);

    printf("Fin\n");

    MPI_Type_free(&type_colonne);
    MPI_Finalize();    
    return EXIT_SUCCESS;
}