#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define M 3
#define N 3

int randInterval(int min, int max){
    return (rand() % (max-min+1)) + min;
}

int main(int argc, char**argv) {
    if(argc < 2){
        printf("Erreur : usage : %s <rang ps root>\n", argv[0]);
    }

    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);
    srand(time(NULL) + rang);

    int entier;
    int root = atoi(argv[1]);

    if(rang == root){
        entier = 42;

        for(int i=0; i<nbProcs; i++){
            if(i != rang) {
                MPI_Send(&entier, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                printf("Envoi de '%d' du ps %d\n", entier, i);
                sleep(1);
            }
        }
    } else {
        MPI_Recv(&entier, 1, MPI_INT, root, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Reçu %d du ps root (%d)\n", entier, root);
    }


    printf("Fin\n");
    MPI_Finalize();    
    return EXIT_SUCCESS;
}