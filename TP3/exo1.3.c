#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define TAILLE 1024
#define MAX 100
#define MIN -100

double moyenne(int tab[], int size){
    int somme = 0;

    for(int i=0; i<size; i++){
        somme += tab[i];
    }

    return (double)somme / size;
}

double moyenneDouble(double tab[], int size){
    double somme = 0.0;

    for(int i=0; i<size; i++){
        somme += tab[i];
    }

    return somme / size;
}

int randInterval(int min, int max){
    return (rand() % (max-min+1)) + min;
}

int main(int argc, char**argv) {
    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

    //srand(time(NULL) + rang);
    srand(0);

    int* mainTab;
    double* moyenneTab;
    int tab[TAILLE];
    double moy;
    double start, end;

    //Init du rang 0
    if(rang == 0){
        mainTab = malloc(nbProcs*TAILLE * sizeof(int));
        for(int i=0; i<nbProcs*TAILLE; i++){
            mainTab[i] = randInterval(MIN, MAX);
        }

        //Allocation de moyenneTab en prévision du Gather
        moyenneTab = malloc(nbProcs * sizeof(double));

        start = MPI_Wtime();
    }
    
    //Scatter
    MPI_Scatter(mainTab, TAILLE, MPI_INT,
        tab, TAILLE, MPI_INT,
        0, MPI_COMM_WORLD);

    for(int i=0; i<TAILLE; i++){
        tab[i] = tab[i]*tab[i]*tab[i];
    }

    // Calcul moyenne
    moy = moyenne(tab, TAILLE);

    //Gather
    MPI_Gather(&moy, 1, MPI_DOUBLE,
        moyenneTab, 1, MPI_DOUBLE,
        0, MPI_COMM_WORLD);
    
    // Finalisation
    if(rang == 0){
        double moyAll = moyenneDouble(moyenneTab, nbProcs);
        printf("Moyenne finale : %lf\n", moyAll);

        end = MPI_Wtime();

        printf("Temps : %lf\n", end-start);

        free(mainTab);
        free(moyenneTab);
    }
    
    printf("Fin\n");

    MPI_Finalize();
    
    return EXIT_SUCCESS;
}