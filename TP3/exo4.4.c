#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "outilsMPI.h"

#define M 10
#define N 4

int main(int argc, char**argv) {
    int rang, nbProcs, tag=0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);
    srand(time(NULL) + rang);

    int voisin;
    MPI_Datatype type_colonne;

    //Init du type
    MPI_Type_vector(M, 1, N, MPI_INT, &type_colonne);
    MPI_Type_commit(&type_colonne);

    printf("Début\n");

    

    if(rang == 0){
        int matrice[M*N];
        for(int i=0; i<M; i++){
            for(int j=0; j<N; j++) {
                matrice[i*N+j] = (rang+1)*10000 + (i+1)*10 + j+1;
            }
        }

        voisin = 1;
        afficheTabInt(matrice, M, N);
        for(int i=0; i<N; i++){
            MPI_Send(matrice + i, 1, type_colonne, voisin, tag, MPI_COMM_WORLD);
        }
    } else if(rang == 1){
        int matrice[M*N];
        voisin = 0;

        for(int i=0; i<N; i++){
            MPI_Recv(matrice + i*M, M, MPI_INT, voisin, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        afficheTabInt(matrice, N, M);
    }


    printf("Fin\n");

    MPI_Type_free(&type_colonne);
    MPI_Finalize();    
    return EXIT_SUCCESS;
}