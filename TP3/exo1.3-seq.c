#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define NB 6
#define TAILLE 1024
#define MAX 100
#define MIN -100

double moyenne(int tab[], int size, int offset){
    int somme = 0;

    for(int i=0; i<size; i++){
        somme += tab[i + offset];
    }

    return (double)somme / size;
}

double moyenneDouble(double tab[], int size){
    double somme = 0.0;

    for(int i=0; i<size; i++){
        somme += tab[i];
    }

    return somme / size;
}

int randInterval(int min, int max){
    return (rand() % (max-min+1)) + min;
}

int main(int argc, char**argv) {
    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

    //srand(time(NULL));
    srand(0);

    int mainTab[NB*TAILLE];
    double moyenneTab[NB];
    double moyAll;
    double start, end;

    //Init du rang 0
    for(int i=0; i<NB*TAILLE; i++){
        mainTab[i] = randInterval(MIN, MAX);
    }

    start = MPI_Wtime();

    //cube
    for(int i=0; i<NB*TAILLE; i++){
        mainTab[i] = mainTab[i]*mainTab[i]*mainTab[i];
    }

    // Calcul moyenne
    for(int i=0; i<NB; i++){
        moyenneTab[i] = moyenne(mainTab, TAILLE, i*TAILLE);
    }
    
    // Finalisation
    moyAll = moyenneDouble(moyenneTab, NB);
    printf("Moyenne finale : %lf\n", moyAll);

    end = MPI_Wtime();

    printf("Temps : %lf\n", end-start);
    
    printf("Fin\n");
    
    MPI_Finalize();
    return EXIT_SUCCESS;
}