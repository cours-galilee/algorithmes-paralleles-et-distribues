#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define TAILLE 1014
#define MAX 200000

int indexMax(int tab[], int size){
    int max = tab[0], index = 0;

    for(int i=1; i<size; i++){
        if(max < tab[i]){
            max = tab[i];
            index = i;
        }
    }

    return index;
}

int main(int argc, char**argv) {
    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

    srand(time(NULL) + rang);

    int tab[TAILLE], index, maxVal, maxAll;

    for(int i=0; i<TAILLE; i++){
        tab[i] = rand() % (MAX+1);
    }

    index = indexMax(tab, TAILLE);
    maxVal = tab[index];

    printf("Max du tableau : %d à l'indice %d\n", maxVal, index);

    MPI_Reduce(&maxVal, &maxAll, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    
    if(rang == 0) {
	    printf("Max de tous les tableaux : %d\n", maxAll);
    }
    
    printf("Fin\n");

    MPI_Finalize();
    
    return EXIT_SUCCESS;
}