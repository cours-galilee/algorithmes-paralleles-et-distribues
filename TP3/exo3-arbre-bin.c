#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define M 3
#define N 3

int randInterval(int min, int max){
    return (rand() % (max-min+1)) + min;
}

int main(int argc, char**argv) {
    int rang, nbProcs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);
    srand(time(NULL) + rang);

    //token : {2^etape, valeur}
    int token[2];
    int root = 0;
    int dest;

    if(rang == root){
        token[0] = 1;
        token[1] = 42;
    } else {
        //TODO
        MPI_Recv(&token, 2, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("%d : Reçu %d\n", rang, token[1]);
        
        token[0] *= 2;
    }

    dest = token[0] + rang;
    while(dest < nbProcs){
        printf("%d : Envoi à %d\n", rang, dest);
        MPI_Send(&token, 2, MPI_INT, dest, 0, MPI_COMM_WORLD);
        sleep(1);

        token[0] *= 2;
        dest = token[0] + rang;
    }


    //printf("Fin\n");
    MPI_Finalize();    
    return EXIT_SUCCESS;
}