/* Demo MPI: hello World 
 * compile with mpicc
 * run with mpirun */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
	char buffer[MPI_MAX_PROCESSOR_NAME];
	int tailleChaine,size,rank;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Get_processor_name(buffer,&tailleChaine);
	printf ("rank %d/%d : Hello World from machine %s\n",rank,size,buffer);

	MPI_Finalize();
	return EXIT_SUCCESS;
}
