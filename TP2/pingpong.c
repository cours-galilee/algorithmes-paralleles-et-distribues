#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char**argv) {
    int rang;
    int jeton = 17, recu;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);


    if( 0 == rang ) {
        MPI_Recv(&recu, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Reçu : %d\n", recu);
    } else if( 1 == rang ) {
        MPI_Send(&jeton, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }

    printf("Fin\n");

    //long int s=0;
    //for(long int i=0; i<10000000000; i++) {
    //    s += i;
    //}
    
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}