#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>


int main(int argc, char**argv) {
    int rang, vois;
    int jeton = 17, recu, tag = 0;
    //MPI_Request req;
    
    MPI_Init(&argc, &argv);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);


    if( 0 == rang ) {
        vois = 1;
        // quoi, quelle taille, quel type, à qui, tag, communicateur
        printf("J'envoie %d\n", jeton);
        MPI_Send(&jeton, 1, MPI_INT, vois, tag, MPI_COMM_WORLD);

        printf("Attente de la réponse...\n");
        MPI_Recv(&recu, 1, MPI_INT, vois, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        printf("Reçu : %d\n", recu);
    } else if( 1 == rang ) {
        vois = 0;
        jeton+=10;

        printf("J'envoie %d\n", jeton);
        MPI_Send(&jeton, 1, MPI_INT, vois, tag, MPI_COMM_WORLD);

        printf("Attente de la réponse...\n");
        MPI_Recv(&recu, 1, MPI_INT, vois, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        printf("Reçu : %d\n", recu);
    }

    printf("Fin\n");

    //fonctionne
    
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}