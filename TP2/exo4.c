#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>

// Le processus k reçois un message de k-1 et envoie à k+1

int main(int argc, char**argv) {
    int rang, nbProcs;
    int jeton = 5, recu, tag = 0;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

    sleep(10);

    //initiateur
    if(rang == 0) {
        // envoie vers 1
        printf("Envoi du jeton '%d' sur l'anneau\n", jeton);
        MPI_Send(&jeton, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);

        printf("Jeton envoyé, attente du retour depuis '%d'...\n", nbProcs-1);

        MPI_Recv(&recu, 1, MPI_INT, nbProcs-1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Jeton '%d' recu !\n", recu);
    } else {
        int prec = rang-1;
        int suiv = (rang+1) % nbProcs;

        printf("Attente d'un jeton...\n");
        
        // réception de k-1
        MPI_Recv(&recu, 1, MPI_INT, prec, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        

        printf("Jeton '%d' recu de %d, envoi de '%d' à %d\n", recu, prec, recu+5, suiv);
        jeton = recu + 5;

        sleep(2);

        // envoi à k+1
        MPI_Send(&jeton, 1, MPI_INT, suiv, tag, MPI_COMM_WORLD);

        printf("Jeton envoyé\n");
    }
    
    printf("Fin\n");

    MPI_Finalize();
    
    return EXIT_SUCCESS;
}