#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>

int main(int argc, char**argv) {
    int taille;
    if(argc < 2) {
        taille = 5000;
    } else {
        taille=atoi(argv[1]);
    }

    //max taille : machines différentes = 65480, local=4040

    printf("Taille du message : %d\n", taille);

    int rang, vois;
    char jeton[taille], recu[taille];
    int tag = 0;

    jeton[taille-1] = '\0';
    //MPI_Request req;
    
    MPI_Init(&argc, &argv);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);


    if( 0 == rang ) {
        vois = 1;
        for(int i=0; i<taille-1; i++){
            jeton[i] = 'A';
        }

        // quoi, quelle taille, quel type, à qui, tag, communicateur
        printf("J'envoie le message\n");
        MPI_Send(&jeton, taille, MPI_CHAR, vois, tag, MPI_COMM_WORLD);

        printf("Attente de la réponse...\n");
        MPI_Recv(&recu, taille, MPI_CHAR, vois, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        printf("Reçu !\n");
    } else if( 1 == rang ) {
        vois = 0;
        for(int i=0; i<taille-1; i++){
            jeton[i] = 'B';
        }

        printf("J'envoie le message\n");
        MPI_Send(&jeton, taille, MPI_CHAR, vois, tag, MPI_COMM_WORLD);

        printf("Attente de la réponse...\n");
        MPI_Recv(&recu, taille, MPI_CHAR, vois, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        printf("Reçu !\n");
    }

    printf("Fin\n");

    //fonctionne correctement
    
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}