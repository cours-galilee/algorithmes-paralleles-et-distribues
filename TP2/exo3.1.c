#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>


int main(int argc, char**argv) {
    int rang, vois;
    int jeton = 17, recu, tag = 0;
    MPI_Request req;
    
    MPI_Init(&argc, &argv);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);


    if( 0 == rang ) {
        vois = 1;
        // quoi, quelle taille, quel type, à qui, tag, communicateur
        printf("J'envoie %d\n", jeton);
        MPI_Isend(&jeton, 1, MPI_INT, vois, tag, MPI_COMM_WORLD, &req);
        MPI_Wait(&req, MPI_STATUS_IGNORE);

        printf("Attente de la réponse...\n");
        MPI_Irecv(&recu, 1, MPI_INT, vois, tag, MPI_COMM_WORLD, &req);
        MPI_Wait(&req, MPI_STATUS_IGNORE);

        printf("Reçu : %d\n", recu);
    } else if( 1 == rang ) {
        vois = 0;
        printf("J'attends l'envoi...\n");
        MPI_Irecv(&recu, 1, MPI_INT, vois, tag, MPI_COMM_WORLD, &req);
        MPI_Wait(&req, MPI_STATUS_IGNORE);

        printf("Reçu : %d\n", recu);
        recu +=10;

        sleep(2);

        printf("J'envoie %d\n", recu);
        MPI_Isend(&recu, 1, MPI_INT, vois, tag, MPI_COMM_WORLD, &req);
        MPI_Wait(&req, MPI_STATUS_IGNORE);
    }

    printf("Fin\n");
    
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}